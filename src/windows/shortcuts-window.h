/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef UKUI_SIDEBAR_SHORTCUTS_WINDOW_H
#define UKUI_SIDEBAR_SHORTCUTS_WINDOW_H

#include <shared-engine-view.h>
#include <window-helper.h>

class QScreen;

namespace Sidebar {
class WindowBlurHelper;
}

class ShortcutsWindow : public UkuiQuick::SharedEngineView
{
    Q_OBJECT
    Q_PROPERTY(bool isTabletMode READ isTabletMode NOTIFY tabletModeChanged)
    Q_PROPERTY(int windowMargin READ windowMargin NOTIFY windowMarginChanged)
public:
    explicit ShortcutsWindow(QWindow *parent = nullptr);
    bool isTabletMode() const;
    int windowMargin() const;
    void activeShortcutsWindow(bool active = true);

    void requestMenuWidget(QString widgetId = "", bool showReturnButton = false, QString returnButtonName = "");
    void backToShortcuts();

public Q_SLOTS:
    void setBlurStrength(quint32 strength = 4000);

Q_SIGNALS:
    void tabletModeChanged();
    void windowMarginChanged();

    void showMenuWidget(QString widgetId, bool showReturnButton, QString returnButtonName);
    void hideMenuWidget();

private Q_SLOTS:
    void updateGeometry();
    void onTabletModeChanged();
    void onPrimaryScreenChanged(QScreen *screen);
    void moveShortcutPanel(int distance);
    void onRight2LeftReleased(int posX, int posY);

protected:
    bool event(QEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    void initUI();
    void initSettings();
    void initConnections();
    void updateEffects();

    bool m_isTabletMode {false};
    int m_panelPos{4};
    int m_panelSize{48};
    int m_panelType{0};
    int m_settingsIslandPosition{0};
    int m_topbarSize{0};
    Qt::LayoutDirection m_layoutDirection{Qt::LayoutDirection::LeftToRight};
    UkuiQuick::WindowProxy2 *m_windowProxy {nullptr};
};


#endif //UKUI_SIDEBAR_SHORTCUTS_WINDOW_H
