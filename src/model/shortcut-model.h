/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-7-28.
//

#ifndef UKUI_SIDEBAR_SHORTCUT_MODEL_H
#define UKUI_SIDEBAR_SHORTCUT_MODEL_H

#include <QMap>
#include <QList>
#include <QAbstractListModel>

#include "ukui-shortcut-plugin.h"

namespace UkuiShortcut {

class ShortcutModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ShortcutModel(PluginMetaType::SystemMode mode, QObject *parent = nullptr);

    /**
     * 按序号插入快捷按键按钮
     */
    bool insertShortcut(Shortcut *shortcut);

    bool removeShortcut(Shortcut *shortcut);

    int rowCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void active(int index, PluginMetaType::Action action);

    /**
     * 给插件设置value，可以反射
     * @brief setValue
     * @param index
     * @param value
     */
    Q_INVOKABLE void setValue(int index, int value);

    /**
     * 前端在拖动进度条时，需要屏蔽插件的value更新，用于保证前端进度条拖动的流畅性
     * @param disable
     */
    Q_INVOKABLE void disableValueUpdate(bool disable);

    /**
     * 更新系统模式后，刷新model数据
     * @param newMode
     */
    void setCurrentMode(PluginMetaType::SystemMode newMode);

    /**
     * 单独刷新model,从新排序
     */
    void updateModel();

Q_SIGNALS:
    void requestExecAction(UkuiShortcut::PluginMetaType::PredefinedAction action);

private Q_SLOTS:
    void updateShortcutStatus(UkuiShortcut::Shortcut* plugin, const UkuiShortcut::StatusInfo &info);

private:
    // 对比发生改变的数据列
    inline void compareDataChanges(QVector<int> &roles, const StatusInfo &infoA, const StatusInfo &infoB) const;
    static void sortAllShortCuts(QList<Shortcut*> &shortcuts, PluginMetaType::SystemMode currentMode);
    inline void execPreAction(UkuiShortcut::Shortcut *shortcut);

private:
    PluginMetaType::SystemMode m_currentMode = PluginMetaType::PC;
    bool m_disableValueUpdate = false;
    int m_shortcutCount = 0;
    //用于前端排序
    QList<Shortcut*> m_shortcuts;
    //用于绑定info
    QMap<Shortcut*, StatusInfo> m_statusInfos;
};

} // UkuiShortcut

#endif //UKUI_SIDEBAR_SHORTCUT_MODEL_H
