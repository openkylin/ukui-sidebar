/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-7-29.
//

#include "shortcut-model-manager.h"
#include "shortcut-model.h"
#include "global-settings.h"

#include <QDebug>
#include <QtQml>

using namespace UkuiShortcut;

ShortcutModelManager::ShortcutModelManager(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<ShortcutModelManager*>("ShortcutModelManager*");
    qRegisterMetaType<ShortcutModel*>("ShortcutModel*");

    updateCurrentMode(TABLET_MODE);
    m_shortcutManager = ShortcutManager::getInstance();

    // init model
    ShortcutModel* iconModel = new ShortcutModel(m_currentMode, this);
    ShortcutModel* menuButtonModel = new ShortcutModel(m_currentMode, this);
    ShortcutModel* progressbarModel = new ShortcutModel(m_currentMode, this);

    m_models.insert(PluginMetaType::Icon, iconModel);
    m_models.insert(PluginMetaType::MenuButton, menuButtonModel);
    m_models.insert(PluginMetaType::ProgressBar, progressbarModel);

    PluginMetaData metaData;
    ShortcutModel *model;
    for (const auto &item : m_shortcutManager->getShortcuts()) {
        metaData = item->pluginMetaData().value(m_currentMode);
        model = getModel(metaData.pluginType());
        if (!model) {
            qDebug() << "can not find the model of xx" << metaData.pluginType();
            continue;
        }

        item->disconnect(this);
        connect(item, &Shortcut::enableStatusChanged, this, [=] (bool isEnable) {
            this->updateShortcutEnable(item, isEnable);
        });

        // 如果插件未启用或者在当前系统模式下不可见，那么将不添加插件到model中，也就是不显示插件
        if (item->isEnable() && metaData.isVisible()) {
            model->insertShortcut(item);
        }
    }

    connect(Sidebar::GlobalSettings::globalInstance(), &Sidebar::GlobalSettings::valueChanged,
            this, &ShortcutModelManager::updateCurrentMode);

    connect(iconModel, &ShortcutModel::requestExecAction, this, &ShortcutModelManager::requestExecAction);
    connect(menuButtonModel, &ShortcutModel::requestExecAction, this, &ShortcutModelManager::requestExecAction);
    connect(progressbarModel, &ShortcutModel::requestExecAction, this, &ShortcutModelManager::requestExecAction);
}

UkuiShortcut::ShortcutModel *ShortcutModelManager::getModel(PluginMetaType::PluginType pluginType)
{
    if (m_models.contains(pluginType)) {
        return m_models.value(pluginType);
    }

    return nullptr;
}

void ShortcutModelManager::updateShortcutEnable(Shortcut *plugin, bool isEnable)
{
    if (!plugin) {
        return;
    }

    if (isEnable) {
        PluginMetaData metaData = plugin->pluginMetaData().value(m_currentMode);
        if (metaData.isVisible()) {
            ShortcutModel *model = getModel(metaData.pluginType());
            if (model) {
                model->insertShortcut(plugin);
            }
        }
    } else {
        // 从model中删除该插件
        for (const auto &model : m_models) {
            if (model->removeShortcut(plugin)) {
                break;
            }
        }
    }
}

void ShortcutModelManager::updateCurrentMode(const QString &key)
{
    if (key == TABLET_MODE) {
        bool isTabletMode = Sidebar::GlobalSettings::globalInstance()->getValue(TABLET_MODE).toBool();
        PluginMetaType::SystemMode oldMode = m_currentMode;
        m_currentMode = isTabletMode ? PluginMetaType::Tablet : PluginMetaType::PC;
        if (oldMode != m_currentMode) {
            updateShortCutModel(oldMode);
        }
    }
}

void ShortcutModelManager::updateShortCutModel(PluginMetaType::SystemMode oldMode)
{
    if (m_models.isEmpty()) {
        return;
    }

    PluginMetaData oldMetaData;
    PluginMetaData currentMetaData;
    ShortcutModel *oldModel;
    ShortcutModel *currentModel;

    // 对每个model从新排序数据
    for (const auto &model : m_models) {
        model->setCurrentMode(m_currentMode);
    }

    for (const auto &item : m_shortcutManager->getShortcuts()) {
        oldMetaData = item->pluginMetaData().value(oldMode);
        currentMetaData = item->pluginMetaData().value(m_currentMode);

        oldModel = getModel(oldMetaData.pluginType());
        // 在当前模式下不可见
        if (!currentMetaData.isVisible() || !item->isEnable()) {
            oldModel->removeShortcut(item);
            continue;
        }

        // 两个模式下的ui类型一致
        if (oldMetaData.pluginType() == currentMetaData.pluginType() && oldMetaData.isVisible()) {
            continue;
        }

        oldModel->removeShortcut(item);
        currentModel = getModel(currentMetaData.pluginType());
        currentModel->insertShortcut(item);
    }
}
