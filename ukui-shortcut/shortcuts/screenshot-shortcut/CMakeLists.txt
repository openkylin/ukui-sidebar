cmake_minimum_required(VERSION 3.16)
project(screenshot-shortcut)

find_package(QT NAMES Qt6 Qt5 COMPONENTS
        Core Gui DBus Widgets LinguistTools
        REQUIRED
        )
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS
        Core Gui DBus Widgets LinguistTools
        REQUIRED
        )

include_directories(../../interface)
include(../../cmake/UkuiShortCommon.cmake)

# 搜索全部源文件
file(GLOB HEADERS ${CMAKE_CURRENT_SOURCE_DIR} "*.h")
file(GLOB SOURCES ${CMAKE_CURRENT_SOURCE_DIR} "*.cpp")

# 查找全部翻译文件 并生成qm文件
file(GLOB TS_FILES ${CMAKE_CURRENT_SOURCE_DIR} "*.ts")
qt5_create_translation(QM_FILES ${CMAKE_CURRENT_SOURCE_DIR} ${TS_FILES})

# 生成动态链接库[.so]文件
add_library(${PROJECT_NAME} SHARED ${HEADERS} ${SOURCES} ${QM_FILES})

set(TRANSLATION_FILE_DIR "${SHORTCUT_TRANSLATION_FILE_DIR}")
# 编译宏定义
target_compile_definitions(${PROJECT_NAME}
        PRIVATE TRANSLATION_FILE_DIR="${TRANSLATION_FILE_DIR}"
        )
# 链接外部库
target_link_libraries(${PROJECT_NAME} PRIVATE
        ukui-shortcut Qt5::Core Qt5::Gui Qt5::DBus Qt5::Widgets
        )

# 安装.so和翻译文件
install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION "${PLUGIN_INSTALL_DIRS}")
install(FILES ${QM_FILES} DESTINATION "${TRANSLATION_FILE_DIR}")
