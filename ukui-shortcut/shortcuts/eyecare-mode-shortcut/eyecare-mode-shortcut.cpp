/*
 * Copyright (C) 2025, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: an-yongtai <anyongtai@kylinos.cn>
 *
 */

#include "eyecare-mode-shortcut.h"
#include <QDebug>
#include <QIcon>

#define SETTINGS_DAEMON_PLUGIN_COLOR          "org.ukui.SettingsDaemon.plugins.color"
#define EYECARE_KEY         "eyeCare"

using namespace UkuiShortcut;
UkuiShortcut::EyeCareModeShortcut::EyeCareModeShortcut(QObject *parent) : Shortcut(parent)
{
    m_currentStatus.setName(tr("eyecare mode"));
    m_currentStatus.setIcon(QIcon::fromTheme("eye-open-negative-filled-symbolic").isNull()
                            ? ":/eyecareModeShortcut/ukui-eyecare.svg" : "eye-open-negative-filled-symbolic");
    m_currentStatus.setToolTip(tr("eyecare mode"));
    m_currentStatus.setColor(Color::ColorRole::BaseColor);
    initMetaData();
    const QByteArray id(SETTINGS_DAEMON_PLUGIN_COLOR);
    if (QGSettings::isSchemaInstalled(id)) {
        m_gsettings = new QGSettings(id);
        if (m_gsettings->keys().contains(EYECARE_KEY, Qt::CaseInsensitive)) {
            setIconButtonColor();
            m_isEnable = true;
        } else {
            qDebug() << "eyecare-mode-shortcut:" << "can't find key :" << EYECARE_KEY;
            m_isEnable = false;
        }
    } else {
        qDebug() << "eyecare-mode-shortcut:" << "can't find gsettings :" << SETTINGS_DAEMON_PLUGIN_COLOR;
        m_isEnable = false;
    }
}

UkuiShortcut::EyeCareModeShortcut::~EyeCareModeShortcut()
{
    if (m_gsettings) {
        delete m_gsettings;
        m_gsettings = nullptr;
    }
}

void EyeCareModeShortcut::onQGSettingsChanged(const QString &key)
{
    if (key == EYECARE_KEY) {
        setIconButtonColor();
        Q_EMIT statusChanged(m_currentStatus);
    }
}

void EyeCareModeShortcut::setIconButtonColor()
{
    if(m_gsettings->get(EYECARE_KEY).toBool()) {
        m_currentStatus.setColor(Color::ColorRole::HighLight);
    } else {
        m_currentStatus.setColor(Color::ColorRole::BaseColor);
    }
}

bool EyeCareModeShortcut::isEnable()
{
    return m_isEnable;
}

void EyeCareModeShortcut::setAddedState(bool added)
{
    if (m_isAdded != added) {
        m_isAdded = added;
        if (!m_gsettings) return;
        if (m_isAdded) {
            connect(m_gsettings, &QGSettings::changed, this,&EyeCareModeShortcut::onQGSettingsChanged);
        } else {
            disconnect(m_gsettings, &QGSettings::changed, this,&EyeCareModeShortcut::onQGSettingsChanged);
        }
    }
}

void EyeCareModeShortcut::active(PluginMetaType::Action action)
{
    if (action == PluginMetaType::Action::Click) {
        trigger();
    }
}

const StatusInfo EyeCareModeShortcut::currentStatus()
{
    return m_currentStatus;
}

QMap<PluginMetaType::SystemMode, PluginMetaData> EyeCareModeShortcut::pluginMetaData()
{
    return m_metaData;
}

void EyeCareModeShortcut::initMetaData()
{
    PluginMetaData pc {true, 9, PluginMetaType::PluginType::Icon};
    pc.setPreAction(PluginMetaType::Hide);
    PluginMetaData tablet {true, 9, PluginMetaType::PluginType::Icon};
    tablet.setPreAction(PluginMetaType::Hide);
    m_metaData.insert(PluginMetaType::SystemMode::PC, pc);
    m_metaData.insert(PluginMetaType::SystemMode::Tablet, tablet);
}

void EyeCareModeShortcut::trigger()
{
    if (!m_isEnable || !m_isAdded) return;
    if (m_gsettings) {
        if (m_gsettings->keys().contains(EYECARE_KEY, Qt::CaseInsensitive)) {
            m_gsettings->set(EYECARE_KEY, !m_gsettings->get(EYECARE_KEY).toBool());
        }
    }
}

EyeCareModeShortcutPlugin::EyeCareModeShortcutPlugin(QObject *parent) : UkuiShortcutPlugin(parent)
{

}

QStringList EyeCareModeShortcutPlugin::translations()
{
    return {QStringLiteral("eyecare-mode-shortcut")};
}

Shortcut *EyeCareModeShortcutPlugin::createShortcut()
{
    return new EyeCareModeShortcut;
}
