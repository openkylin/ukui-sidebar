<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>UkuiShortcut::StatusChangeShortcut</name>
    <message>
        <source>Max Tablet</source>
        <translation>Max Tablette</translation>
    </message>
    <message>
        <source>Tablet Mode</source>
        <translation>Tablet-Modus</translation>
    </message>
</context>
</TS>
