/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#include "brightness-shortcut.h"

#include <QDebug>
#include <QTranslator>
#include <QApplication>
#include <QDBusReply>
#include <QDBusInterface>

using namespace UkuiShortcut;

BrightnessShortcut::BrightnessShortcut(QObject *parent) : Shortcut(parent)
{
    initMetaData();
    init();
    m_currentStatusInfo.setToolTip(tr("Brightness"));
}

BrightnessShortcut::~BrightnessShortcut() = default;

QString BrightnessShortcut::pluginId()
{
    return QStringLiteral("BrightnessShortcut");
}

void BrightnessShortcut::active(PluginMetaType::Action action)
{
    Q_UNUSED(action);
}

void BrightnessShortcut::setValue(int value)
{
    if (m_isEnable) {
        if (m_interface && m_interface->isValid()) {
            m_interface->asyncCall("setPrimaryBrightness", static_cast<quint32>(value));
        }
    }
}

const StatusInfo BrightnessShortcut::currentStatus()
{
    return m_currentStatusInfo;
}

bool BrightnessShortcut::isEnable()
{
    return m_isEnable;
}

void BrightnessShortcut::changeValue(quint32 brightness)
{
    if(m_currentStatusInfo.getValue() == brightness) {
        return;
    }

    updateStatusInfo(brightness);
}

void BrightnessShortcut::updateStatusInfo(quint32 brightness)
{
    if(brightness <= 100) {
        if(brightness == 0) {
            m_currentStatusInfo.setIcon("ukui-light-0-symbolic");
        } else if (brightness <= 25) {
            m_currentStatusInfo.setIcon("ukui-light-25-symbolic");
        } else if (brightness <= 50) {
            m_currentStatusInfo.setIcon("ukui-light-50-symbolic");
        } else if (brightness <= 75) {
            m_currentStatusInfo.setIcon("ukui-light-75-symbolic");
        } else {
            m_currentStatusInfo.setIcon("ukui-light-100-symbolic");
        }
        m_currentStatusInfo.setValue(static_cast<int>(brightness));

    } else {
        qWarning() << "Brightness value error: " << brightness;
        m_currentStatusInfo.setDisable(true);
        m_currentStatusInfo.setValue(0);
        m_currentStatusInfo.setIcon("ukui-light-0-symbolic");
    }

    Q_EMIT statusChanged(m_currentStatusInfo);
}

QMap<PluginMetaType::SystemMode, PluginMetaData> BrightnessShortcut::pluginMetaData()
{
    return m_metaData;
}

void BrightnessShortcut::initMetaData()
{
    PluginMetaData pc {true, 0, PluginMetaType::PluginType::ProgressBar};
    PluginMetaData tablet {true, 0, PluginMetaType::PluginType::ProgressBar};
    m_metaData.insert(PluginMetaType::SystemMode::PC, pc);
    m_metaData.insert(PluginMetaType::SystemMode::Tablet, tablet);
}

void BrightnessShortcut::init()
{
    QString service = "org.ukui.SettingsDaemon";
    QString path = "/GlobalBrightness";
    QString interface = "org.ukui.SettingsDaemon.Brightness";

    m_interface = new QDBusInterface(service, path, interface, QDBusConnection::sessionBus(), this);
    if (m_interface->isValid()) {
        QDBusReply<bool> isEnable = m_interface->call("isEnable");
        if (isEnable.isValid()) {
            m_isEnable = isEnable.value();
        }

        QDBusReply<quint32> v = m_interface->call("getPrimaryBrightness");
        changeValue(v.isValid() ? v.value() : 0);

        QDBusConnection::sessionBus().connect(service, path, interface, "enableChanged", this, SLOT(onEnableChanged(bool)));
        QDBusConnection::sessionBus().connect(service, path, interface, "primaryBrightnessChangedEnd", this, SLOT(changeValue(quint32)));
    }
}

void BrightnessShortcut::onEnableChanged(bool enable)
{
    if (m_isEnable == enable) {
        return;
    }

    m_isEnable = enable;
    if (m_isEnable) {
        QDBusReply<quint32> v = m_interface->call("getPrimaryBrightness");
        changeValue(v.isValid() ? v.value() : 0);
    }

    Q_EMIT enableStatusChanged(m_isEnable);
}

BrightnessShortcutPlugin::BrightnessShortcutPlugin(QObject *parent) : UkuiShortcutPlugin(parent)
{

}

QStringList BrightnessShortcutPlugin::translations()
{
    return {QStringLiteral("brightness-shortcut")};
}

Shortcut *BrightnessShortcutPlugin::createShortcut()
{
    return new BrightnessShortcut;
}
