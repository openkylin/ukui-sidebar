/*
 * Copyright (C) 2025, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: an-yongtai <anyongtai@kylinos.cn>
 *
 */

#include "dark-mode-shortcut.h"
#include <QDebug>
#include <QIcon>

#define SETTINGS_DAEMON_PLUGIN_STYLE      "org.ukui.style"
#define DARKMODE_KEY         "styleName"

using namespace UkuiShortcut;
UkuiShortcut::DarkModeShortcut::DarkModeShortcut(QObject *parent) : Shortcut(parent)
{
    m_currentStatus.setName(tr("dark mode"));
    m_currentStatus.setIcon(QIcon::fromTheme("").isNull()
                            ? ":/darkmodeShortcut/ukui-dark.svg" : "");
    m_currentStatus.setToolTip(tr("dark mode"));
    m_currentStatus.setColor(Color::ColorRole::BaseColor);
    initMetaData();
    const QByteArray id(SETTINGS_DAEMON_PLUGIN_STYLE);
    if (QGSettings::isSchemaInstalled(id)) {
        m_gsettings = new QGSettings(id);
        if (m_gsettings->keys().contains(DARKMODE_KEY, Qt::CaseInsensitive)) {
            setIconButtonColor();
            m_isEnable = true;
        } else {
            qWarning() << "dark-mode-shortcut:" << "can't find key :" << DARKMODE_KEY;
            m_isEnable = false;
        }
    } else {
        qWarning() << "dark-mode-shortcut:" << "can't find gsettings :" << SETTINGS_DAEMON_PLUGIN_STYLE;
        m_isEnable = false;
    }
}

UkuiShortcut::DarkModeShortcut::~DarkModeShortcut()
{
    if (m_gsettings) {
        delete m_gsettings;
        m_gsettings = nullptr;
    }
}

void DarkModeShortcut::setIconButtonColor()
{
    if (m_gsettings->get(DARKMODE_KEY).toString() == "ukui-dark") {
        m_currentStatus.setColor(Color::ColorRole::HighLight);
    } else if (m_gsettings->get(DARKMODE_KEY).toString() == "ukui-light") {
        m_currentStatus.setColor(Color::ColorRole::BaseColor);
    }
}

void DarkModeShortcut::onQGSettingsChanged(const QString &key)
{
    if (key == DARKMODE_KEY) {
        setIconButtonColor();
        Q_EMIT statusChanged(m_currentStatus);
    }
}

bool DarkModeShortcut::isEnable()
{
    return m_isEnable;
}

void DarkModeShortcut::setAddedState(bool added)
{
    if (m_isAdded != added) {
        m_isAdded = added;
        if (!m_gsettings) return;
        if (m_isAdded) {
            connect(m_gsettings, &QGSettings::changed, this, &DarkModeShortcut::onQGSettingsChanged);
        } else {
            disconnect(m_gsettings, &QGSettings::changed, this, &DarkModeShortcut::onQGSettingsChanged);
        }
    }
}

void DarkModeShortcut::active(PluginMetaType::Action action)
{
    if (action == PluginMetaType::Action::Click) {
        trigger();
    }
}

const StatusInfo DarkModeShortcut::currentStatus()
{
    return m_currentStatus;
}

QMap<PluginMetaType::SystemMode, PluginMetaData> DarkModeShortcut::pluginMetaData()
{
    return m_metaData;
}

void DarkModeShortcut::initMetaData()
{
    PluginMetaData pc {true, 8, PluginMetaType::PluginType::Icon};
    PluginMetaData tablet {true, 8, PluginMetaType::PluginType::Icon};
    m_metaData.insert(PluginMetaType::SystemMode::PC, pc);
    m_metaData.insert(PluginMetaType::SystemMode::Tablet, tablet);
}

void DarkModeShortcut::trigger()
{
    if (!m_isEnable || !m_isAdded) return;
    if (m_gsettings) {
        if (m_gsettings->keys().contains(DARKMODE_KEY, Qt::CaseInsensitive)) {
            QString styleName = m_gsettings->get(DARKMODE_KEY).toString();
            m_gsettings->set(DARKMODE_KEY, styleName == "ukui-dark" ? "ukui-light" : "ukui-dark");
        }
    }
}

DarkModeShortcutPlugin::DarkModeShortcutPlugin(QObject *parent) : UkuiShortcutPlugin(parent)
{

}

QStringList DarkModeShortcutPlugin::translations()
{
    return {QStringLiteral("dark-mode-shortcut")};
}

Shortcut *DarkModeShortcutPlugin::createShortcut()
{
    return new DarkModeShortcut;
}
