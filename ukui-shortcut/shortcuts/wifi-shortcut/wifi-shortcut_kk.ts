<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>Wi-Fi جالعانباعان</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>جالعانباعان</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>ياپچان</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>Wi-Fi</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>جالعانباعان</translation>
    </message>
</context>
</TS>
