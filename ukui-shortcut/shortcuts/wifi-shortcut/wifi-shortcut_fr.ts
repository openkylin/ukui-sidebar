<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>Le Wi-Fi est connecté</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>Le Wi-Fi n’est pas connecté</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>Le Wi-Fi n’est pas disponible</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>Wi-Fi</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>Le Wi-Fi n’est pas connecté</translation>
    </message>
</context>
</TS>
