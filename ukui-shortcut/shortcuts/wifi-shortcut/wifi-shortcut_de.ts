<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>Wi-Fi ist verbunden</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>Wi-Fi ist nicht verbunden</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>WLAN ist nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>Wi-Fi (Englisch)</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>Wi-Fi ist nicht verbunden</translation>
    </message>
</context>
</TS>
