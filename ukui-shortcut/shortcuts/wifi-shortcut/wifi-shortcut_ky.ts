<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>WiFi ۇلانباعان</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>ۇلانباعان</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>ياپچان</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>Wi-Fi</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>ۇلانباعان</translation>
    </message>
</context>
</TS>
