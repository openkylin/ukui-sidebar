<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>無線網絡已連接</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>未鏈接</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>無線網絡不可用</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>無線局域網</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>無網路連接</translation>
    </message>
</context>
</TS>
