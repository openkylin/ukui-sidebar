<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>سىملىق تورغا ئۇلاش</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>ئۇلانمىغان</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>ياپچان</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>WI-FI</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>ئۇلانمىغان</translation>
    </message>
</context>
</TS>
