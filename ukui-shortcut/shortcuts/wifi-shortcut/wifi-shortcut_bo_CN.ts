<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>སྐུད་མེད་དྲ་བ་སྦྲེལ་ཡོད།</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>སྐུད་མེད་དྲ་བ་འབྲེལ་མཐུད་བྱས་མེད།</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>སྐུད་མེད་དྲ་རྒྱ་བཀོལ་སྤྱོད་བྱེད་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>སྐུད་མེད་དྲ་བ་འབྲེལ་མཐུད་བྱས་མེད།</translation>
    </message>
</context>
</TS>
