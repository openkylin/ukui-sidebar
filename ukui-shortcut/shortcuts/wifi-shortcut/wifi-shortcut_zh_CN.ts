<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>无线网络已连接</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>无线网络未连接</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>无线网络不可用</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>Not connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>网络</translation>
    </message>
</context>
</TS>
