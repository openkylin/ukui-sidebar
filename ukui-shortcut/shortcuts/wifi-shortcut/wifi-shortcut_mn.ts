<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translation>ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠵᠠᠢ</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠣᠯᠪᠣᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation>ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠲᠣᠣᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠶᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translation>ᠤᠲᠠᠰᠤ ᠦᠭᠡᠶ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠦ ᠲᠣᠤᠷ</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation>ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠣᠯᠪᠣᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
</context>
</TS>
