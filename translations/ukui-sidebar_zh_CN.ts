<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>还有%1则通知</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>通知中心</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>没有新通知</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>清空</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>通知中心</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>没有收到新通知</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>还有%1则通知</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>折叠</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>one notification from %1</source>
        <translation>来自%1的1条通知</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>还有%1则通知</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>管理员</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>普通用户</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>显示侧边栏现在的状态。</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>notify -&amp;gt;打开通知中心，control -&amp;gt; 打开控制中心。</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>退出侧边栏。</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>侧边栏</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>快捷操作</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>现在</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>昨天 </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>快捷操作</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>通知中心</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>%1 条通知</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>电源</translation>
    </message>
</context>
</TS>
