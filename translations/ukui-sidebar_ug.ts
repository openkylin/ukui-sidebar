<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>يەنە %1 ئۇقتۇرۇش قىلىنىدۇ</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>ئۇقتۇرۇش مەركىزى</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>يېڭى ئۇقتۇرۇش يوق</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>بىكار قىلىش</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>ئۇقتۇرۇش مەركىزى</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>يېڭى ئۇقتۇرۇش تاپشۇرۇۋالمىدى</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>يەنە %1 ئۇقتۇرۇش قىلىنىدۇ</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>قاتلىماق</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>يەنە %1 ئۇقتۇرۇش قىلىنىدۇ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>باشقۇرغۇچى</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>ئادەتتىكى خېرىدار</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>يان تەرەپتىكى رېشاتكىنىڭ ھازىرقى ھالىتىنى كۆرسىتىش</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>notify-&amp;gt; ئۇقتۇرۇش مەركىزىنى ئاچتى، control-&amp;gt; كونترول مەركىزىنى ئاچتى</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>يان تەرەپتىكى رېشاتكىدىن چېكىنىپ</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>يان تەرەپ رېشاتكىسى</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>تېز مەشغۇلات قىلىش</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>ھازىر</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>تۈنۈگۈن </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>تېز مەشغۇلات قىلىش</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>ئۇقتۇرۇش مەركىزى</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>1%بىر پارچە ئۇقتۇرۇش</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>توك مەنبەسى</translation>
    </message>
</context>
</TS>
