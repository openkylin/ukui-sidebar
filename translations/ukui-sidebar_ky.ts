<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>داعى %1 ۇقتۇرۇۇ جاسالات</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>ۇقتۇرۇۇ بوربورۇ</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>جاڭى ۇقتۇرۇۇ جوق</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>بەكەر  جاسوو ،اتقارۇۇ</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>ۇقتۇرۇۇ بوربورۇ</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>جاڭى ۇقتۇرۇۇ تاپشۇرۇۋالمىدى</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>داعى %1 ۇقتۇرۇۇ جاسالات</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>قاتلىماق</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>بەكىتىش</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>داعى %1 ۇقتۇرۇۇ جاسالات</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>باشقارعۇۇچۇ</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>اداتتاعى سووداگەر ، جولووچۇ</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>ۇقۇق ،امال  جاعىنداقى  رېشاتكىنىڭ ازىرقى  ابالىن كۅرسۅتۉۉ</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>notify-&amp;gt; ۇقتۇرۇۇ بوربورۇن اچتى، control-&amp;gt; تىزگىندۅۅ  بوربورۇن اچتى</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>ۇقۇق ،امال  جاعىنداقى  رېشاتكىدىن جانىپ</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>ۇقۇق ،امال  تاراپ قاشاسى</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>تەز ماشقۇلدانۇۇ  جاسوو ،اتقارۇۇ</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>ازىر</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>كەچە </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>تەز ماشقۇلدانۇۇ  جاسوو ،اتقارۇۇ</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>ۇقتۇرۇۇ بوربورۇ</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>%1 بىر   پارچا ۇقتۇرۇۇ</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>تۅك قاينارى</translation>
    </message>
</context>
</TS>
