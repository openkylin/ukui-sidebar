<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>Il existe également une notification %1</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>Centre de notifications</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>Pas de nouvelles notifications</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>vide</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>Centre de notifications</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>Aucune nouvelle notification n’a été reçue</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>Il existe également une notification %1</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>plier</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>Arrêter</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>Il existe également une notification %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>administrateur</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>Utilisateurs réguliers</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>Affiche l’état actuel de la barre latérale.</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>Notifier -&amp;gt; ouvre le centre de notifications, et Contrôle -&amp;gt; ouvre le centre de contrôle.</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>Quittez la barre latérale.</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>Barre latérale</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>Actions rapides</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>Tout de suite</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>Hier </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>Actions rapides</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>Centre de notifications</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>%1 notifications</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>alimentation</translation>
    </message>
</context>
</TS>
