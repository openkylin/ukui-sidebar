<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>جانە %1 ۇقتٸرۋ قىلىندى</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>ۇقتٸرۋ ورتالىعى</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>جاڭا ۇقتٸرۋ جوق</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>بوس ەتۋ</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>ۇقتٸرۋ ورتالىعى</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>جاڭا ۇقتٸرۋ تاپشۇرۇۋالمىدى</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>جانە %1 ۇقتٸرۋ قىلىندى</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>قوسىلماق</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>تاقاۋ</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>جانە %1 ۇقتٸرۋ قىلىندى</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>باسقارۋشٸسٸ</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>ادەتتەگٸ قاريدار</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>جان جاقتاداعٸ رېشاتكىنىڭ قازىرعى كۇيىن كورسەتۋ</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>notify-&amp;gt; ۇقتٸرۋ ورتالىقتى اشتٸ، control-&amp;gt; مەڭگەرۋ ورتالىقتى اشتٸ</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>جان جاقتاداعٸ رېشاتكىدىن شەگنىپ</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>جان جاق راشەتكاسى</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>تەز جوبالاۋ ەتۋ</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>قازىر</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>كەشە </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>تەز جوبالاۋ ەتۋ</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>ۇقتٸرۋ ورتالىعى</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>%1 بٸر پارشا ۇقتٸرۋ</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>توك قاينارى</translation>
    </message>
</context>
</TS>
