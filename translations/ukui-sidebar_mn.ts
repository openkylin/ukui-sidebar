<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>ᠪᠠᠰᠠ 1 ᠮᠡᠳᠡᠭᠳᠡᠯ ᠪᠠᠶᠢᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>ᠲᠥᠪ ᠲᠦ ᠮᠡᠳᠡᠭᠳᠡ</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>ᠲᠥᠪ ᠲᠦ ᠮᠡᠳᠡᠭᠳᠡ</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠢ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>ᠪᠠᠰᠠ 1 ᠮᠡᠳᠡᠭᠳᠡᠯ ᠪᠠᠶᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>ᠨᠤᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>ᠪᠠᠰᠠ 1 ᠮᠡᠳᠡᠭᠳᠡᠯ ᠪᠠᠶᠢᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>ᠡᠩ ᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠶᠢᠨ ᠬᠠᠵᠠᠭᠤ ᠳᠠᠬᠢ ᠣᠳᠣ ᠶᠢᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>notixy &amp;gt; ᠮᠡᠳᠡᠭᠳᠡᠯ ᠦᠨ ᠲᠥᠪ ᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠂ control &amp;gt; ᠡᠵᠡᠮᠳᠡᠯ ᠦᠨ ᠲᠥᠪ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠠᠴᠠ ᠭᠠᠷᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠶᠢᠨ ᠬᠠᠵᠠᠭᠤ ᠳᠠᠬᠢ ᠬᠠᠰᠢᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>ᠲᠦᠷᠭᠡᠨ ᠠᠵᠢᠯᠯᠠ</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>ᠣᠳᠣ</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>ᠥᠴᠥᠭᠡᠳᠦᠷ </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>ᠲᠦᠷᠭᠡᠨ ᠠᠵᠢᠯᠯᠠ</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>ᠲᠥᠪ ᠲᠦ ᠮᠡᠳᠡᠭᠳᠡ</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>1 ᠵᠦᠢᠯ ᠦᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ᠃</translation>
    </message>
</context>
</TS>
