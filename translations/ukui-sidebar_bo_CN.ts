<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>ད་དུང་བརྒྱ་ཆ་1ལ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>ལྟེ་གནས་ལ་བརྡ་ཐོ་གཏོང</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>བརྡ་ཐོ་གསར་པ་མེད།</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>དྭངས་ཤིང་གཙང་བ།</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>ལྟེ་གནས་ལ་བརྡ་ཐོ་གཏོང</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>བརྡ་ཐོ་གསར་པ་འབྱོར་མེད།</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>ད་དུང་བརྒྱ་ཆ་1ལ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>ལྟེབ་བརྩེགས་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>ད་དུང་བརྒྱ་ཆ་1ལ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>དོ་དམ་པ།</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>སྤྱོད་མཁན་དཀྱུས་མ།</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>ཟུར་ངོས་ཀྱི་ད་ལྟའི་རྣམ་པ་མངོན་པ་རེད།</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>nottf-&amp;gt;་ཁ་ཕྱེ་ནས་བརྡ་ཐོ་ལྟེ་གནས་ཕྱེ་བ་དང་། conorrororroe &amp;gt;tretretrest།</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>ལོགས་ངོས་ཀྱི་ར་བ་ལས་ཕྱིར་འབུད་པ།</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>ལོགས་ངོས་ཀྱི་ར་བ།</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>མགྱོགས་མྱུར་གྱིས་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>ད་ལྟ་</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>ཁ་སང་། </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>མགྱོགས་མྱུར་གྱིས་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>ལྟེ་གནས་ལ་བརྡ་ཐོ་གཏོང</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>བརྡ་ཐོ་བརྒྱ་ཆ་1བཅས་གཏོང་དགོས།</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>གློག་ཁུངས།</translation>
    </message>
</context>
</TS>
