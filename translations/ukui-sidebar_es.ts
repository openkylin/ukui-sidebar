<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>También hay una notificación %1</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>Centro de notificaciones</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>No hay nuevas notificaciones</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>vacío</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>Centro de notificaciones</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>No se han recibido nuevas notificaciones</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>También hay una notificación %1</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>doblar</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>Apaga</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>También hay una notificación %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation>administrador</translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>Usuarios habituales</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>Muestra el estado actual de la barra lateral.</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>Notificar -&amp;gt; abre el Centro de notificaciones y Control -&amp;gt; abre el Centro de control.</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>Sal de la barra lateral.</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>Barra lateral</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>Acciones rápidas</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>Ahora mismo</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>Ayer </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>Acciones rápidas</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>Centro de notificaciones</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>%1 Notificaciones</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>fuente de alimentación</translation>
    </message>
</context>
</TS>
