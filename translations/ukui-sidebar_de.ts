<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>CollapsedList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>Es gibt auch eine %1-Benachrichtigung</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <source>Notification Center</source>
        <translation>Mitteilungszentrale</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>Keine neuen Benachrichtigungen</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>leer</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <source>Notification Center</source>
        <translation>Mitteilungszentrale</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <source>No new notifications received</source>
        <translation>Es sind keine neuen Benachrichtigungen eingegangen</translation>
    </message>
</context>
<context>
    <name>NotificationList</name>
    <message>
        <source>%1 more notifications</source>
        <translation>Es gibt auch eine %1-Benachrichtigung</translation>
    </message>
    <message>
        <source>Show less</source>
        <translation>falten</translation>
    </message>
</context>
<context>
    <name>PopupNotificationItem</name>
    <message>
        <source>Close</source>
        <translation>Herunterfahren</translation>
    </message>
</context>
<context>
    <name>PopupView</name>
    <message>
        <source>%1 more notifications</source>
        <translation>Es gibt auch eine %1-Benachrichtigung</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Administrator</source>
        <translation></translation>
    </message>
    <message>
        <source>Standard user</source>
        <translation>Regelmäßige Benutzer</translation>
    </message>
    <message>
        <source>Show the current state of the sidebar.</source>
        <translation>Zeigt den aktuellen Status der Seitenleiste an.</translation>
    </message>
    <message>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>Benachrichtigen -&amp;gt; öffnet die Mitteilungszentrale und Strg -&amp;gt; öffnet das Kontrollzentrum.</translation>
    </message>
    <message>
        <source>Quit sidebar.</source>
        <translation>Verlassen Sie die Seitenleiste.</translation>
    </message>
    <message>
        <source>ukui-sidebar</source>
        <translation>Seitenleiste</translation>
    </message>
</context>
<context>
    <name>ShortcuPanel</name>
    <message>
        <source>Shortcuts</source>
        <translation>Schnelle Aktionen</translation>
    </message>
</context>
<context>
    <name>Sidebar::DateTimeUtils</name>
    <message>
        <source>Now</source>
        <translation>Jetzt gerade</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>Gestern </translation>
    </message>
</context>
<context>
    <name>SidebarMain</name>
    <message>
        <source>Shortcuts</source>
        <translation>Schnelle Aktionen</translation>
    </message>
    <message>
        <source>NotificationCenter</source>
        <translation>Mitteilungszentrale</translation>
    </message>
    <message>
        <source>%1 Notifications</source>
        <translation>%1 Benachrichtigungen</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <source>Power</source>
        <translation>Stromversorgung</translation>
    </message>
</context>
</TS>
