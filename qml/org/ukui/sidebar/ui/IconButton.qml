/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.shortcut.core 1.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

BaseButton {
    UkuiItems.Background {
        anchors.fill: parent

        radius: mainWindow.isTabletMode ? Platform.Theme.maxRadius*2 : Platform.Theme.normalRadius

        mixMode: UkuiItems.ColorMix.Normal
        backColorRole: buttonColor === PluginColorRole.HighLight ? Platform.Theme.Highlight : Platform.Theme.Base
        backColorAlpha: (buttonColor === PluginColorRole.HighLight) ? 1 : Platform.Theme.isDarkTheme ? 0.45 : 0.75
        foreColorRole: Platform.Theme.BrightText
        foreColorAlpha: mouseArea.containsPress ? 0.15 : mouseArea.containsMouse ? 0.05 : 0

        border.width: 1
        borderColor: Platform.Theme.BrightText
        borderAlpha: 0.05

        MouseArea {
            id: mouseArea
            hoverEnabled: true
            anchors.fill: parent
            onClicked: {
                active(ShortcutPlugin.Click)
            }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.topMargin: 8
            spacing: 4

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true

                UkuiItems.Icon {
                    width: mainWindow.isTabletMode ? 32 : 24
                    height: mainWindow.isTabletMode ? 32 : 24
                    anchors.centerIn: parent
                    source: icon
                    mode: (buttonColor === PluginColorRole.HighLight) ? UkuiItems.Icon.Highlight :
                        (buttonColor === PluginColorRole.Disable) ? UkuiItems.Icon.Disabled : UkuiItems.Icon.AutoHighlight
                }
            }

            UkuiItems.StyleText {
                opacity: disabled ? 0.45 : 1
                Layout.fillWidth: true
                Layout.preferredHeight: contentHeight + 8

                text: name
                elide: Qt.ElideRight
                paletteRole: (buttonColor === PluginColorRole.HighLight) ? Platform.Theme.HighlightedText : Platform.Theme.ButtonText

                pointSizeOffset: -2
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignTop
            }
        }
    }
}
