/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.shortcut.core 1.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

Item {
    Column {
        id: panelLayout
        width: parent.width
        height: childrenRect.height

        Item {
            width: parent.width
            height: 56

            RowLayout {
                anchors.fill: parent
                anchors.margins: 4

                UkuiItems.StyleText {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    text: qsTr("Shortcuts")
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }

                UkuiItems.Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: height
                    Layout.alignment: Qt.AlignVCenter

                    background.radius: mainWindow.isTabletMode ? Platform.Theme.maxRadius : Platform.Theme.normalRadius
                    background.paletteRole: Platform.Theme.WindowText
                    background.alpha: containsPress ? 0.16 : containsMouse ? 0.08 : 0.0
                    icon.source: "applications-system-symbolic"
                    icon.mode: UkuiItems.Icon.AutoHighlight

                    onClicked: {
                        appManager.launchApp("/usr/share/applications/ukui-control-center.desktop", "ukui-control-center");
                    }
                }
            }
        }

        BaseButtonView {
            width: parent.width
            columns: 2
            buttoonType: ShortcutPlugin.MenuButton
            buttonComponent: MenuButton {}
        }

        BaseButtonView {
            width: parent.width
            buttoonType: ShortcutPlugin.Icon
            buttonComponent: IconButton {}
        }

        BaseButtonView {
            width: parent.width
            columns: 1
            buttoonType: ShortcutPlugin.ProgressBar
            buttonComponent: ProgressBar {}
        }
    }
}
