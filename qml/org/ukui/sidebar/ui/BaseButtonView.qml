/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.shortcut.core 1.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

GridView {
    id: baseView
    property int margins: 4
    property int columns: 4
    readonly property int rows: Math.ceil(count / columns)
    property int buttoonType: ShortcutPlugin.Icon
    property Component buttonComponent: undefined

    height: rows * cellHeight
    interactive: false
    cellWidth: Math.floor(width / columns)
    cellHeight: (mainWindow.isTabletMode ? 104 : 72) + margins*2

    model: modelManager.getModel(buttoonType)
    delegate: Item {
        width: baseView.cellWidth
        height: baseView.cellHeight

        Loader {
            anchors.fill: parent
            anchors.centerIn: parent
            anchors.margins: baseView.margins

            // properties
            property int index: model.index
            property int buttonColor: model.color
            property int buttonValue: model.value
            property bool disabled: model.disabled
            property string icon: model.icon
            property string name: model.name
            property string statusName: model.statusName
            property string widgetId: model.widgetId
            property string tooltip: model.tooltip

            sourceComponent: baseView.buttonComponent

            function activeButton(act) {
                baseView.model.active(index, act)
            }
            function setValue(val) {
                baseView.model.setValue(index, val)
            }
            function setDisableValueUpdate(disable) {
                baseView.model.disableValueUpdate(disable)
            }

            onLoaded: {
                item.active.connect(activeButton)
                item.updateValue.connect(setValue)
                item.disableValueUpdate.connect(setDisableValueUpdate)
            }

            Component.onDestruction: {
                item.active.disconnect(activeButton)
                item.updateValue.disconnect(setValue)
                item.disableValueUpdate.disconnect(setDisableValueUpdate)
            }
        }
    }
}
