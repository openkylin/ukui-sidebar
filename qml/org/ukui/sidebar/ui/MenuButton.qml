/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.shortcut.core 1.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

BaseButton {
    UkuiItems.Background {
        anchors.fill: parent
        radius: mainWindow.isTabletMode ? Platform.Theme.maxRadius*2 : Platform.Theme.normalRadius

        backColorRole: Platform.Theme.Base
        backColorAlpha: Platform.Theme.isDarkTheme ? 0.45 : 0.75

        border.width: 1
        borderColor: Platform.Theme.BrightText
        borderAlpha: 0.05

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: mainWindow.isTabletMode ? 24 : 12
            anchors.rightMargin: 12
            spacing: 4

            UkuiItems.Background {
                Layout.preferredWidth: mainWindow.isTabletMode ? 64 : 48
                Layout.preferredHeight: mainWindow.isTabletMode ? 64 : 48
                Layout.alignment: Qt.AlignVCenter

                radius: width/2

                mixMode: UkuiItems.ColorMix.Normal
                backColorRole: buttonColor === PluginColorRole.HighLight ? Platform.Theme.Highlight : Platform.Theme.Base
                backColorAlpha: (buttonColor === PluginColorRole.HighLight) ? 1 : Platform.Theme.isDarkTheme ? 0.45 : 0.75
                foreColorRole: Platform.Theme.BrightText
                foreColorAlpha: mouseArea.containsPress ? 0.15 : mouseArea.containsMouse ? 0.05 : 0

                border.width: 1
                borderColor: Platform.Theme.BrightText
                borderAlpha: 0.05

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        active(ShortcutPlugin.Click);
                    }
                }

                UkuiItems.Icon {
                    width: parent.width/2
                    height: parent.width/2
                    anchors.centerIn: parent
                    source: icon
                    // mode: UkuiItems.Icon.AutoHighlight
                    mode: (buttonColor === PluginColorRole.HighLight) ? UkuiItems.Icon.Highlight :
                        (buttonColor === PluginColorRole.Disable) ? UkuiItems.Icon.Disabled : UkuiItems.Icon.AutoHighlight
                }
            }

            UkuiItems.StyleText {
                Layout.fillWidth: true
                Layout.fillHeight: true

                text: statusName
                font.pixelSize: 16
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft

                MouseArea {
                    anchors.fill: parent
                    onClicked: widgetPage.showWidget(widgetId, true, name)
                }
            }

            UkuiItems.Icon {
                Layout.preferredWidth: 24
                Layout.preferredHeight: 24
                source: "ukui-end-symbolic"
                mode: UkuiItems.Icon.AutoHighlight

                MouseArea {
                    anchors.fill: parent;
                    onClicked: widgetPage.showWidget(widgetId, true, name)
                }
            }
        }
    }
}
