/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.shortcut.core 1.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

BaseButton {
    UkuiItems.Background {
        anchors.fill: parent
        radius: mainWindow.isTabletMode ? Platform.Theme.maxRadius*2 : Platform.Theme.normalRadius

        backColorRole: Platform.Theme.Base
        backColorAlpha: Platform.Theme.isDarkTheme ? 0.45 : 0.75

        border.width: 1
        borderColor: Platform.Theme.BrightText
        borderAlpha: 0.05

        RowLayout {
            spacing: 8
            anchors.fill: parent
            anchors.leftMargin: mainWindow.isTabletMode ? 24 : 12
            anchors.rightMargin: mainWindow.isTabletMode ? 24 : 12

            UkuiItems.Icon {
                Layout.preferredWidth: mainWindow.isTabletMode ? 32 : 24
                Layout.preferredHeight: mainWindow.isTabletMode ? 32 : 24
                Layout.alignment: Qt.AlignVCenter
                source: icon
                mode: UkuiItems.Icon.AutoHighlight

                MouseArea {
                    property bool hovered: false;
                    anchors.fill: parent
                    hoverEnabled: true;
                    ToolTip.visible: hovered;
                    ToolTip.text: tooltip
                    onEntered: {
                        hovered = true;
                    }
                    onExited: {
                        hovered = false;
                    }
                    onClicked: {
                        active(ShortcutPlugin.Click);
                    }
                }
            }

            MouseArea {
                id: controlBase;

                Layout.fillWidth: true;
                Layout.preferredHeight: mainWindow.isTabletMode ? 40 : 24;
                Layout.alignment: Qt.AlignVCenter;

                onWheel: function (e) {
                    if (e.angleDelta.y > 0) {
                        control.increaseValue();
                    } else if (e.angleDelta.y < 0) {
                        control.decreaseValue();
                    } else if (e.angleDelta.x < 0) {
                        control.increaseValue();
                    } else if (e.angleDelta.x > 0) {
                        control.decreaseValue();
                    }
                }

                Slider {
                    id: control;
                    anchors.fill: parent;
                    value: buttonValue / 100;
                    padding: 0;
                    stepSize:0.01;
                    onPressedChanged: {
                        disableValueUpdate(pressed)
                    }

                    onValueChanged: {
                        if (pressed) {
                            updateValue(value * 100);
                        }
                    }
                    Keys.onLeftPressed: decreaseValue()
                    Keys.onUpPressed: increaseValue()
                    Keys.onRightPressed: increaseValue()
                    Keys.onDownPressed: decreaseValue()

                    function increaseValue() {
                        increase();
                        updateValue(value * 100);
                    }

                    function decreaseValue() {
                        decrease();
                        updateValue(value * 100);
                    }

                    //进度条
                    background: UkuiItems.StyleBackground {
                        id: sliderBackground;
                        implicitWidth: controlBase.width;
                        implicitHeight: controlBase.height;

                        radius: mainWindow.isTabletMode ? 12 : 8
                        useStyleTransparency: false;
                        paletteRole: Platform.Theme.Button

                        UkuiItems.StyleBackground {
                            id: top;
                            width: LayoutMirroring.enabled
                                   ? (1 - control.visualPosition) * (control.availableWidth - (radius * 2 + whiteSpot.width)) + (radius * 2 + whiteSpot.width)
                                   : control.visualPosition * (control.availableWidth - (radius * 2 + whiteSpot.width)) + (radius * 2 + whiteSpot.width);
                            height: parent.height;
                            anchors.left: parent.left;
                            radius: sliderBackground.radius;
                            useStyleTransparency: false;
                            paletteRole: Platform.Theme.Highlight;

                            Rectangle {
                                id: whiteSpot;
                                width: top.radius / 2;
                                height: top.radius;
                                color: "white";
                                radius: 5;

                                anchors.right: top.right;
                                anchors.rightMargin: top.radius;
                                anchors.verticalCenter: top.verticalCenter;
                            }
                        }
                    }
                    //滑块
                    handle: Item {
                        id: sliderHandel;
                        x: control.visualPosition * (control.availableWidth - width);
                        y: control.availableHeight / 2 - height / 2;
                        implicitWidth: controlBase.height;
                        implicitHeight: controlBase.height;
                    }
                }
            }

            Item {
                Layout.preferredWidth: 24
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignVCenter
                visible: mainWindow.isTabletMode ? false : true

                UkuiItems.Icon {
                    anchors.fill: parent
                    source: "ukui-end-symbolic"
                    mode: UkuiItems.Icon.AutoHighlight
                    visible: widgetId === "" ? false : true

                    MouseArea {
                        anchors.fill: parent;
                        onClicked: widgetPage.showWidget(widgetId, true, name)
                    }
                }
            }
        }
    }
}
