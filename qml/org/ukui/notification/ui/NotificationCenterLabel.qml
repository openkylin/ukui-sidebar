/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

//PC侧边栏顶部显示
Item {
    id: sidebarTips;
    property bool notificationEmpty: false
    signal clearClick();

    UkuiItems.StyleText {
        anchors.verticalCenter: parent.verticalCenter;
        anchors.left: parent.left;
        verticalAlignment: Text.AlignVCenter;
        font.bold: true;
        text: qsTr("Notification Center");
        paletteRole: Platform.Theme.ButtonText;
    }

    MouseArea {
        id: sidebarAllClear
        width: 40
        height: 32
        visible: !notificationEmpty

        anchors.right: sidebarSystemSetting.left;
        anchors.rightMargin: 1;
        anchors.verticalCenter: parent.verticalCenter;

        hoverEnabled: true

        CurvedItem {
            anchors.fill: parent
            layer.enabled: isOpenGLEnv
            layer.samples: 4
            cornersRadius: LayoutMirroring.enabled ?
                               [0,Platform.Theme.normalRadius,Platform.Theme.normalRadius,0] :
                               [Platform.Theme.normalRadius,0,0,Platform.Theme.normalRadius]
            UkuiItems.ThemeIcon {
                anchors.centerIn: parent
                width: 16
                height: 16
                mode: UkuiItems.Icon.AutoHighlight
                source: "edit-delete-symbolic"
            }
            Component.onCompleted: {
                updateColor();
            }
            Platform.Theme.onPaletteChanged: {
                updateColor();
            }
            function updateColor() {
                color = Qt.binding(getColor);
            }
            function getColor() {
                return Qt.tint(Platform.Theme.colorWithCustomTransparency(Platform.Theme.Base, Platform.Theme.Active, 0.75),
                               Platform.Theme.colorWithCustomTransparency(Platform.Theme.BrightText, Platform.Theme.Active,
                                                                           sidebarAllClear.containsPress ?
                                                                           0.15 : sidebarAllClear.containsMouse ? 0.05 : 0));
            }
        }
        onClicked: {
            sidebarTips.clearClick();
        }
    }

    MouseArea {
        id: sidebarSystemSetting
        width: 40
        height: 32

        anchors.right: parent.right;
        anchors.verticalCenter: parent.verticalCenter;

        hoverEnabled: true

        CurvedItem {
            anchors.fill: parent
            layer.enabled: isOpenGLEnv
            layer.samples: 4
            cornersRadius: notificationEmpty ?
                               [Platform.Theme.normalRadius,
                                Platform.Theme.normalRadius,
                                Platform.Theme.normalRadius,
                                Platform.Theme.normalRadius] : LayoutMirroring.enabled ?
                                   [Platform.Theme.normalRadius,0,0,Platform.Theme.normalRadius] :
                                   [0,Platform.Theme.normalRadius,Platform.Theme.normalRadius,0]
            UkuiItems.ThemeIcon {
                anchors.centerIn: parent
                width: 16
                height: 16
                mode: UkuiItems.Icon.AutoHighlight
                source: "system-settings-symbolic"
            }
            Component.onCompleted: {
                updateColor();
            }
            Platform.Theme.onPaletteChanged: {
                updateColor();
            }
            function updateColor() {
                color = Qt.binding(getColor);
            }
            function getColor() {
                return Qt.tint(Platform.Theme.colorWithCustomTransparency(Platform.Theme.Base, Platform.Theme.Active, 0.75),
                               Platform.Theme.colorWithCustomTransparency(Platform.Theme.BrightText, Platform.Theme.Active,
                                                                           sidebarSystemSetting.containsPress ?
                                                                           0.15 : sidebarSystemSetting.containsMouse ? 0.05 : 0));
            }
        }
        onClicked: {
            appManager.launchAppWithArguments("/usr/share/applications/ukui-control-center.desktop", ["-m", "Notice"], "ukui-control-center");
        }
    }
}
