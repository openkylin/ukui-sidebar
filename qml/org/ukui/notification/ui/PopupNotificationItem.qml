/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

MouseArea {
    id: root
    property alias tabletTips: tabletTips
    property alias pcTips: pcTips
    property int itemMargin: 16
    property alias background: background
    property var actions: model.actions
    property bool screenLock
    signal actionExecuted(string action)
    signal notificationClosed()
    signal clearButtonAreaEntered(bool isEnter)

    hoverEnabled: true
    UkuiItems.StyleBackground {
        id: background
        useStyleTransparency: false
        paletteRole: Platform.Theme.Base
        height: itemColumn.height + itemMargin*2
        width: root.width
        alpha: 0.85
        radius: 12
        // radius: Platform.Theme.normalRadius
        // borderColor: Platform.Theme.Text
        // borderAlpha: 0.1
        // border.width: 1

        Column {
            id: itemColumn
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: root.width - itemMargin*2
            height: childrenRect.height
            spacing: 8

            RowLayout {
                width: parent.width
                height: 24
                spacing: 8

                Loader {
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.height
                    sourceComponent: Component {
                       UkuiItems.Icon {
                            source: model.image === undefined ? model.appIconName : model.image
                        }
                    }
                }

                UkuiItems.StyleText {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    text: model.appName
                    alpha: 0.54

                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }
                UkuiItems.StyleText {
                    id: timeStamp
                    Layout.fillHeight: true
                    Layout.preferredWidth: contentWidth
                    text: dateTimeUtils.computeTimeOut(model.createTime)
                    visible: !clearButton.visible
                    alpha: 0.54

                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    Component.onCompleted: {
                        dateTimeUtils.timeRefresh.connect(refreshTimeStamp);
                        dateTimeUtils.dateUpdate.connect(refreshTimeStamp);
                        dateTimeUtils.timeUpdate.connect(refreshTimeStamp);
                    }
                    Component.onDestruction: {
                        dateTimeUtils.timeRefresh.disconnect(refreshTimeStamp);
                        dateTimeUtils.dateUpdate.disconnect(refreshTimeStamp);
                        dateTimeUtils.timeUpdate.disconnect(refreshTimeStamp)
                    }
                    function refreshTimeStamp() {
                        timeStamp.text = dateTimeUtils.computeTimeOut(model.createTime);
                    }
                }
                UkuiItems.StyleBackground {
                    id: clearButton
                    Layout.preferredWidth: 24
                    Layout.fillHeight: true
                    radius: 12
                    useStyleTransparency: false
                    paletteRole: Platform.Theme.Button
                    visible: !model.isStored || (model.isStored && root.containsMouse)
                    alpha: clearButtonMouse.containsPress ? 1 : clearButtonMouse.containsMouse ? 0.80 : 0

                    UkuiItems.Icon {
                        height: 16
                        width: 16
                        anchors.centerIn: parent
                        mode: UkuiItems.Icon.AutoHighlight
                        source: "window-close-symbolic"

                        ToolTip.visible: clearButtonMouse.containsMouse
                        ToolTip.delay: 500
                        ToolTip.text: qsTr("Close")
                    }
                    MouseArea {
                        id: clearButtonMouse
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: {
                            root.notificationClosed();
                        }
                        onEntered: {
                            root.clearButtonAreaEntered(true);
                        }
                        onExited: {
                            root.clearButtonAreaEntered(false);
                        }
                    }
                }
            }

            ColumnLayout {
                width: parent.width - 64
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 4

                UkuiItems.StyleText {
                    Layout.alignment: Qt.AlignLeft
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: contentHeight
                    text: model.summary

                    maximumLineCount: 2
                    font.bold: true
                    wrapMode: Text.Wrap
                    verticalAlignment: Text.AlignTop
                    horizontalAlignment: Text.AlignLeft
                    elide: Text.ElideRight

                    MouseArea {
                        width: parent.contentWidth
                        height: parent.height
                        visible: parent.truncated
                        hoverEnabled: true

                        onPressed: (e) => { e.accepted = false; }

                        Tooltip {
                            visible: parent.containsMouse
                            text: model.summary
                        }
                    }
                }
                UkuiItems.StyleText {
                    Layout.alignment: Qt.AlignLeft
                    Layout.preferredWidth: parent.width
                    text: screenLock ?
                              model.showContentOnLockScreen ?
                                  model.body : qsTr("one notification from %1").arg(model.appName): model.body
                    maximumLineCount: 2
                    wrapMode: Text.Wrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    elide: Text.ElideRight
                    onLinkActivated: function(link) {
                        if (Qt.openUrlExternally(link)) {
                            if (!model.resident) {
                                root.notificationClosed();
                            }
                        } else {
                            console.log("onLinkActivated: link error, ", link, Qt.resolvedUrl(link));
                        }
                    }

                    MouseArea {
                        width: parent.contentWidth
                        height: parent.height
                        visible: parent.truncated
                        hoverEnabled: true

                        onPressed: (e) => { e.accepted = false; }

                        Tooltip {
                            visible: parent.containsMouse
                            text: model.body
                        }
                    }
                }
                UkuiItems.StyleText {
                    id: pcTips
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: contentHeight
                    Layout.alignment: Qt.AlignLeft
                    alpha: 0.54
                    visible: false

                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }

                RowLayout {
                    Layout.preferredWidth: parent.width
                    Layout.alignment: Qt.AlignLeft
                    height: actionView.count === 0 && !tabletTips.visible ? 0 : 36
                    spacing: 0

                    UkuiItems.StyleText {
                        id: tabletTips
                        Layout.fillHeight: true
                        Layout.preferredWidth: contentWidth
                        alpha: 0.54
                        visible: false

                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                    }

                    Item {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }

                    Component {
                        id: actionButtonDelegate
                        MouseArea {
                            id: buttonMouseArea
                            hoverEnabled: true
                            property string buttonIcon: ""
                            property string buttonText: modelData.name
                            property int radius: 4
                            width: 98
                            height: ListView.view.height
                            enabled: modelData.isEnable

                            onClicked: {
                                root.actionExecuted(modelData.action);
                            }

                            UkuiItems.StyleBackground {
                                id: buttonBase
                                useStyleTransparency: false
                                paletteRole: Platform.Theme.Button
                                anchors.fill: parent
                                radius: buttonMouseArea.radius
                                alpha: buttonMouseArea.containsPress ? 1 : buttonMouseArea.containsMouse ? 0.80 : 0.5
                                paletteGroup: modelData.isEnable ? Platform.Theme.Active : Platform.Theme.Disabled
                            }

                            RowLayout {
                                anchors.fill: parent
                                anchors.margins: 8

                                UkuiItems.ThemeIcon {
                                    id: themeIcon
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft // default
                                    Layout.preferredWidth: 16
                                    Layout.preferredHeight: 16
                                    source: buttonMouseArea.buttonIcon
                                    visible: buttonMouseArea.buttonIcon !== ""
                                    mode: UkuiItems.Icon.AutoHighlight
                                }

                                UkuiItems.StyleText {
                                    Layout.fillWidth: true
                                    Layout.preferredHeight: contentHeight
                                    Layout.maximumHeight: parent.height
                                    text: buttonMouseArea.buttonText
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                    elide: Text.ElideRight
                                }
                            }
                            states: State {
                                when: buttonMouseArea.activeFocus
                                PropertyChanges {
                                    target: buttonBase
                                    borderColor: Platform.Theme.Highlight
                                    border.width: 2
                                }
                            }
                        }
                    }

                    ListView {
                        id: actionView
                        Layout.fillHeight: true
                        Layout.preferredWidth: contentWidth
                        spacing: 8
                        interactive: false
                        orientation: ListView.Horizontal

                        model: {
                            var acs = [];
                            for (var i = 0; i < actions.length; ++i) {
                                if (!actions[i].isDefault && acs.length < 2) {
                                    acs.push(actions[i]);
                                }
                            }

                            return acs;
                        }
                        delegate: actionButtonDelegate
                    }
                }
            }
        }
    }
}
