/*
 * Copyright (C) 2024, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.12
import QtQml 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.ukui.sidebar.ui 1.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.shortcut.core 1.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

UkuiItems.StyleBackground {
    id: root
    readonly property int padding: mainWindow.isTabletMode ? 16 : 8
    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true
    paletteRole: Platform.Theme.Window
    radius: Platform.Theme.windowRadius
    focus: true

    Keys.onEscapePressed: {
        if (mainWindow.visible) {
            if (mainWindow.isTabletMode) {
                root.activePanelOnTablet(false);
            } else {
                mainWindow.hide();
            }
        }
    }

    function moveShortcutPanel(dx) {
        if (LayoutMirroring.enabled) {
            let newX = dx - panelParent.width - padding;
            panel.animationX = newX > 0 ? 0 : newX;
        } else {
            let newX = panelParent.width + padding - dx;
            panel.animationX = newX < 0 ? 0 : newX;
        }
    }

    function initTabletProp() {
        panel.animationX = LayoutMirroring.enabled
                ? -(panelParent.width + padding)
                : panelParent.width + padding;
    }

    function activePanelOnTablet(active) {
        if (panelAnimation.running) {
            return;
        }

        panelAnimation.active = active;
        panelAnimation.from = panel.animationX;
        panelAnimation.to = active
                ? 0
                : LayoutMirroring.enabled
                  ? -(panelParent.width + root.padding)
                  : (panelParent.width + root.padding);
        panelAnimation.start();
    }

    NumberAnimation {
        id: panelAnimation
        property bool active: true
        target: panel
        property: "animationX"
        duration: 300
        easing.type: Easing.InOutQuad

        onFinished: {
            if (!active) {
                mainWindow.hide();
            }
        }
    }

    MouseArea {
        anchors.fill: parent

        onClicked: {
            if (mainWindow.isTabletMode) {
                root.activePanelOnTablet(false);
            } else {
                //mainWindow.hide();
            }
        }
    }

    Binding {
        target: root
        property: "width"
        when: !mainWindow.isTabletMode
        value: panelParent.width + root.padding*2
        restoreMode: Binding.RestoreNone
    }

    Binding {
        target: root
        property: "height"
        when: !mainWindow.isTabletMode
        value: panelParent.height + root.padding*2
        restoreMode: Binding.RestoreNone
    }

    SwipeView {
        id: panelParent

        z: 10
        x: mainWindow.isTabletMode
           ? LayoutMirroring.enabled
             ? root.padding : (root.width - width - root.padding) : root.padding
        y: mainWindow.isTabletMode ? (root.height - height - root.padding) : root.padding
        clip: true
        interactive: false

        ShortcuPanel {
            id: panel

            property real animationX: LayoutMirroring.enabled ? -(panelParent.width + root.padding) : 0
            x: 0
            Binding on x {
                when: mainWindow.isTabletMode
                value: panel.animationX
                restoreMode: Binding.RestoreValue
            }

            width: mainWindow.isTabletMode ? 540 : 396
            height: childrenRect.height

            onWidthChanged: {
                panelParent.width = width;
                widgetPage.width = width;
            }

            onHeightChanged: {
                panelParent.height = height;
                widgetPage.height = height;
            }

            onXChanged: {
                let w = width + root.padding;
                mainWindow.setBlurStrength(Math.floor(4000*(1 - x/w)));
            }
        }

        Item {
            id: widgetPage
            property bool showReturnButton
            property string returnButtonName

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                RowLayout {
                    Layout.preferredWidth: 364
                    Layout.maximumWidth: 364
                    Layout.preferredHeight: 32
                    Layout.maximumHeight: 32
                    Layout.alignment: Qt.AlignHCenter
                    spacing: 4
                    visible: widgetPage.showReturnButton

                    UkuiItems.Icon {
                        Layout.preferredWidth: 24
                        Layout.preferredHeight: 24
                        Layout.alignment: Qt.AlignVCenter
                        source: "ukui-start-symbolic"
                        mode: UkuiItems.Icon.AutoHighlight

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                widgetPage.hideWidget();
                            }
                        }
                    }

                    UkuiItems.StyleText {
                        Layout.fillHeight: true

                        text: widgetPage.returnButtonName === "" ?
                                  qsTr("back to Shortcuts") : widgetPage.returnButtonName
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                widgetPage.hideWidget();
                            }
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }

                WidgetDelegate {
                    id: widget

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    quickWidgetId: ""
                }
            }

            function showWidget(id, show, name) {
                widget.quickWidgetId = id;
                widgetPage.showReturnButton = show;
                widgetPage.returnButtonName = name;
                panelParent.currentIndex = 1;
            }
            function hideWidget() {
                widget.quickWidgetId = "";
                panelParent.currentIndex = 0;
            }
        }

        Connections {
            target: mainWindow
            function onShowMenuWidget(id, show, name) {
                widgetPage.showWidget(id, show, name)
            }
        }

        Connections {
            target: mainWindow
            function onHideMenuWidget() {
                widgetPage.hideWidget()
            }
        }
    }
}
